﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyAppVer2
{
    /// <summary>
    /// Interaction logic for PatientWindow.xaml
    /// </summary>
    public partial class PatientWindow : Window
    {
        public User myUser = new User();
        public List<Patient> allPatients = new List<Patient>();
        public PatientWindow(User myUser)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.myUser = myUser;
            allPatients = DataManager.GetAllPatients();
            foreach (Patient x in allPatients)
            {
                lbListPatienten.Items.Add(x);
            }
        }

        private void btnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAddNewPatient_Click(object sender, RoutedEventArgs e)
        {
            PatientCreateWindow thispatientCreateWindow = new PatientCreateWindow();
            thispatientCreateWindow.ShowDialog();
            if (thispatientCreateWindow.DialogResult.HasValue && thispatientCreateWindow.DialogResult.Value)
            {
                UpdatePatients();
            }
        }

        private void btnSearchOnePatient_Click(object sender, RoutedEventArgs e)
        {
            if (txtSearchBox.Text != "")
            {
                List<Patient> thesePatients = DataManager.GetPatientByName(txtSearchBox.Text);
                lbListPatienten.ItemsSource = null;
                lbListPatienten.Items.Clear();
                lbListPatienten.ItemsSource = thesePatients;
            }
            else
            {
                MessageBox.Show("Gelieve een Naam in te geven", "Hola", MessageBoxButton.OK);
            }
        }

        private void btnListPatients_Click(object sender, RoutedEventArgs e)
        {
            lbListPatienten.ItemsSource = null;
            lbListPatienten.Items.Clear();
            allPatients = DataManager.GetAllPatients();
            foreach (Patient x in allPatients)
            {
                lbListPatienten.Items.Add(x);
            }
        }

        private void btnDetail_Click(object sender, RoutedEventArgs e)
        {
            Patient myPatient = new Patient();
            if (lbListPatienten.SelectedItem != null)
            {
                myPatient = lbListPatienten.SelectedItem as Patient;
                PatientDetailWindow thispatientDetailWindow = new PatientDetailWindow(myPatient, myUser);
                thispatientDetailWindow.ShowDialog();
            }
        }
        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            Patient myPatient = new Patient();
            if (lbListPatienten.SelectedItem != null)
            {
                myPatient = lbListPatienten.SelectedItem as Patient;
                PatientUpdateWindow thispatientUpdateWindow = new PatientUpdateWindow(myPatient, myUser);
                thispatientUpdateWindow.ShowDialog();
                if (thispatientUpdateWindow.DialogResult.HasValue && thispatientUpdateWindow.DialogResult.Value)
                {
                    UpdatePatients();
                }
            }
        }
        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (lbListPatienten.SelectedItem != null)
            {
                Patient myPatient = new Patient();
                myPatient = lbListPatienten.SelectedItem as Patient;
                if (MessageBox.Show($"Wilt u Patient {myPatient.PatientLastName} {myPatient.PatientFirstName} verwijderen?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    DataManager.DeletePatient(myPatient);
                    MessageBox.Show("Succesvol verwijderd", "Geslaagd", MessageBoxButton.OK);
                    lbListPatienten.Items.Clear();
                    lbListPatienten.ItemsSource = null;
                    allPatients = DataManager.GetAllPatients();
                    foreach (Patient x in allPatients)
                    {
                        lbListPatienten.Items.Add(x);
                    }
                }
            }
        }
        private void btnAddSession_Click(object sender, RoutedEventArgs e)
        {
            if (lbListPatienten.SelectedItem != null)
            {
                SessionCreateWindow thisSessionCreateWindow = new SessionCreateWindow(lbListPatienten.SelectedItem as Patient, myUser);
                thisSessionCreateWindow.ShowDialog();
                if (thisSessionCreateWindow.DialogResult.HasValue && thisSessionCreateWindow.DialogResult.Value)
                {
                    MessageBox.Show($"Sessie Aangemaakt voor Patient", "Succes", MessageBoxButton.OK);
                }
            }
            else
            {
                MessageBox.Show("Selecteer een Patient", "Error", MessageBoxButton.OK);
            }
        }
        private void UpdatePatients()
        {
            lbListPatienten.Items.Clear();
            lbListPatienten.ItemsSource = null;
            allPatients = DataManager.GetAllPatients();
            foreach (Patient x in allPatients)
            {
                lbListPatienten.Items.Add(x);
            }
        }
    }
}
