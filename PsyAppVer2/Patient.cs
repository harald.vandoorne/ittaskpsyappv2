namespace PsyAppVer2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Patient")]
    public partial class Patient
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Patient()
        {
            Session = new HashSet<Session>();
        }

        public int PatientID { get; set; }

        [Required]
        [StringLength(50)]
        public string PatientFirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string PatientLastName { get; set; }

        [StringLength(50)]
        public string PatientStreet { get; set; }

        [StringLength(10)]
        public string PatientHouseNumber { get; set; }

        [StringLength(10)]
        public string PatientZipcode { get; set; }

        [StringLength(10)]
        public string PatientCity { get; set; }

        [StringLength(20)]
        public string PatientPhone { get; set; }

        [StringLength(50)]
        public string PatientEmail { get; set; }

        [StringLength(50)]
        public string PatientReferral { get; set; }

        [StringLength(50)]
        public string PatientReferralReason { get; set; }

        [StringLength(500)]
        public string PatientComments { get; set; }

        public bool PatientIsCompany { get; set; }

        [StringLength(50)]
        public string PatientCompanyNumber { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Session> Session { get; set; }

        public override string ToString()
        {
            return $"{PatientFirstName}       {PatientLastName}       {PatientStreet}";
        }
    }
}
