namespace PsyAppVer2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("User")]
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            Session = new HashSet<Session>();
        }

        public int UserID { get; set; }

        [Required]
        public bool UserIsAdmin { get; set; }
        
        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        [StringLength(50)]
        public string UserAdress { get; set; }

        [Required]
        [StringLength(10)]
        public string UserHouseNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string UserCity { get; set; }

        [Required]
        [StringLength(12)]
        public string UserZipcode { get; set; }

        [Required]
        [StringLength(50)]
        public string UserPhone { get; set; }

        [Required]
        [StringLength(50)]
        public string UserEmail { get; set; }

        [Required]
        [StringLength(50)]
        public string UserBankAccount { get; set; }

        [Required]
        [StringLength(50)]
        public string UserTaxNumber { get; set; }

        [Required]
        [StringLength(50)]
        public string UserPassword { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Session> Session { get; set; }
    }
}
