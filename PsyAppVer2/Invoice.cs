namespace PsyAppVer2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Invoice")]
    public partial class Invoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Invoice()
        {
            Session = new HashSet<Session>();
        }

        public int InvoiceID { get; set; }

        public int PatientID { get; set; }

        public int SessionID { get; set; }

        public int UserID { get; set; }

        public bool InvoiceIsPaid { get; set; }

        [Required]
        [StringLength(50)]
        public string InvoicePaymentMethod { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Session> Session { get; set; }
    }
}
