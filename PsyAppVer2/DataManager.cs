﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PsyAppVer2
{
    public static class DataManager
    {
        // Datamanager Class met alle CRUD functies voor DB
        public static List<Patient> GetAllPatients()
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                return PsyAppDBEntities.Patient.ToList();
            }
        }

        public static List<Patient> GetPatientByName(string name)
        {
            List<Patient> mylist = new List<Patient>();
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                foreach (Patient x in PsyAppDBEntities.Patient.ToList())
                {
                    if (x.PatientLastName == name)
                    {
                        mylist.Add(x);
                    }
                }
                return mylist;
            }
        }

        public static void CreatePatient(Patient x)
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                PsyAppDBEntities.Patient.Add(x);
                PsyAppDBEntities.SaveChanges();
            }
        }

        public static string UpdatePatient(Patient x)
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                PsyAppDBEntities.Entry(x).State = System.Data.Entity.EntityState.Modified;
                PsyAppDBEntities.SaveChanges();
                return x.PatientLastName;
            }
        }

        public static string DeletePatient(Patient x)
        {
            Patient y = x;
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                var query = from Patient in PsyAppDBEntities.Patient
                            where Patient.PatientID == y.PatientID
                            select Patient;
                Patient z = query.FirstOrDefault();
                if (z != null)
                {
                    PsyAppDBEntities.Patient.Remove(z);
                }
                // OF PatientDBEntities.Entry(x).State = System.Data.Entity.EntityState.Deleted;
                PsyAppDBEntities.SaveChanges();
                return x.PatientLastName;
            }
        }
        public static List<Session> GetAllSessions()
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                return PsyAppDBEntities.Session.ToList();
            }
        }
        public static Session GetSessionByID(int x)
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                foreach (Session y in PsyAppDBEntities.Session.ToList())
                {
                    if (y.SessionID == x)
                    {
                        return y;
                    }
                }
                return null;
            }
        }
        public static List<Session> GetSessionsByDate(DateTime x)
        {
            List<Session> mylist = new List<Session>();
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                foreach (Session y in PsyAppDBEntities.Session.ToList())
                {
                    if (x == y.SessionDate)
                    {
                        mylist.Add(y);
                    }
                }
                return mylist;
            }
        }
        public static List<Session> GetSessionsByPatient(Patient x)
        {
            List<Session> mylist = new List<Session>();
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                foreach (Session y in PsyAppDBEntities.Session.ToList())
                {
                    if (x.PatientID == y.PatientID)
                    {
                        mylist.Add(y);
                    }
                }
                return mylist;
            }
        }

        public static string CreateSession(Session x)
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                PsyAppDBEntities.Patient.Attach(x.Patient);
                PsyAppDBEntities.Day.Attach(x.Day);
                PsyAppDBEntities.User.Attach(x.User);
                PsyAppDBEntities.Session.Add(x);
                if (0 < PsyAppDBEntities.SaveChanges())
                {
                    return x.SessionSubject;

                }
                return "Niet Gelukt";
            }
        }
        public static string UpdateSession(Session x)
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                PsyAppDBEntities.Entry(x).State = System.Data.Entity.EntityState.Modified;
                PsyAppDBEntities.Patient.Attach(x.Patient);
                PsyAppDBEntities.Day.Attach(x.Day);
                PsyAppDBEntities.SaveChanges();
                return x.SessionSubject;
            }
        }

        public static string DeleteSession(Session x)
        {
            Session y = x;
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                var query = from Session in PsyAppDBEntities.Session
                            where Session.SessionID == y.SessionID
                            select Session;
                Session z = query.FirstOrDefault();
                if (z != null)
                {
                    PsyAppDBEntities.Patient.Attach(z.Patient);
                    PsyAppDBEntities.Day.Attach(z.Day);
                    PsyAppDBEntities.Session.Remove(z);
                }
                // OF PatientDBEntities.Entry(x).State = System.Data.Entity.EntityState.Deleted;
                PsyAppDBEntities.SaveChanges();
                return x.SessionSubject;
            }
        }
        public static List<Day> GetAllDays()
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                return PsyAppDBEntities.Day.ToList();
            }
        }
        public static Day GetDayByDate(DateTime x)
        {
            Day myday = new Day();
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                foreach (Day y in PsyAppDBEntities.Day.ToList())
                {
                    if (y.DayDate == x)
                    {
                        return y;
                    }
                }
                return myday;
            }
        }
        public static Day GetDayBySession(Session x)
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                Day myday = new Day();
                foreach (Day y in PsyAppDBEntities.Day.ToList())
                {
                    if (y.Session.FirstOrDefault(z => z.SessionID == x.SessionID) != default)
                    {
                        return y;
                    }
                }
                return myday;
            }
        }
        public static DateTime? CreateDay(Day x)
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                PsyAppDBEntities.Day.Add(x);
                if (0 < PsyAppDBEntities.SaveChanges())
                {
                    return x.DayDate;

                }
                return null;
            }
        }
        public static DateTime UpdateDay(Day x)
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                PsyAppDBEntities.Entry(x).State = System.Data.Entity.EntityState.Modified;
                PsyAppDBEntities.SaveChanges();
                return x.DayDate;
            }
        }
        public static User GetUser(string Username)
        {
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                User thisUser = PsyAppDBEntities.User.FirstOrDefault(x => x.UserName == Username);
                if (thisUser == null)
                {
                    return null;
                }
                return thisUser;
            }
        }
    }
}
