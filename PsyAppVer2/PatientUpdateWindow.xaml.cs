﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyAppVer2
{
    /// <summary>
    /// Interaction logic for PatientUpdateWindow.xaml
    /// </summary>
    public partial class PatientUpdateWindow : Window
    {
        Patient myPatient = new Patient();
        List<Session> mySessions = new List<Session>();
        User myUser = new User();
        public PatientUpdateWindow(Patient myPatient, User myUser)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.myPatient = myPatient;
            this.mySessions = DataManager.GetSessionsByPatient(myPatient);
            mySessions.OrderBy(p => p.SessionDate);
            cmbCompany.Items.Add("Privé");
            cmbCompany.Items.Add("Bedrijf");
            UpdateInput(myPatient);
            this.myUser = myUser;
        }

        private void btnSavePatient_Click(object sender, RoutedEventArgs e)
        {
            myPatient.PatientCity = txtCity.Text;
            myPatient.PatientStreet = txtStreet.Text;
            myPatient.PatientFirstName = txtVoornaam.Text;
            myPatient.PatientLastName = txtFamilienaam.Text;
            myPatient.PatientHouseNumber = txtNumber.Text;
            myPatient.PatientPhone = txtPhone.Text;
            myPatient.PatientEmail = txtEmail.Text;
            myPatient.PatientReferral = txtReferral.Text;
            myPatient.PatientReferralReason = txtReason.Text;
            myPatient.PatientZipcode = txtZip.Text;
            myPatient.PatientComments = txtComments.Text;
            lbSessions.ItemsSource = mySessions;
            if (cmbCompany.SelectedIndex == 1)
            {
                myPatient.PatientIsCompany = true;
                myPatient.PatientCompanyNumber = txtCompany.Text;
            }
            else
            {
                myPatient.PatientIsCompany = false;
            }
            DataManager.UpdatePatient(myPatient);
            this.DialogResult = true;
            this.Close();
        }

        private void btnPatientMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAddSession_Click(object sender, RoutedEventArgs e)
        {
            SessionCreateWindow mySessionCreateWindow = new SessionCreateWindow(myPatient, myUser);
            mySessionCreateWindow.ShowDialog();
            if (mySessionCreateWindow.DialogResult.HasValue && mySessionCreateWindow.DialogResult.Value)
            {
                MessageBox.Show($"Sessie Aangemaakt voor Patient", "Succes", MessageBoxButton.OK);
                lbSessions.ItemsSource = null;
                mySessions = DataManager.GetSessionsByPatient(myPatient);
                lbSessions.ItemsSource = mySessions;
            }
        }

        private void btnDetailSession_Click(object sender, RoutedEventArgs e)
        {
            SessionDetailWindow mySessionDetailWindow = new SessionDetailWindow(lbSessions.SelectedItem as Session, myUser);
            mySessionDetailWindow.ShowDialog();
        }
        private void UpdateInput(Patient x)
        {
            txtCity.Text = x.PatientCity.ToString();
            txtStreet.Text = x.PatientStreet.ToString();
            txtVoornaam.Text = x.PatientFirstName.ToString();
            txtFamilienaam.Text = x.PatientLastName.ToString();
            txtNumber.Text = x.PatientHouseNumber.ToString();
            txtPhone.Text = x.PatientPhone.ToString();
            txtEmail.Text = x.PatientEmail.ToString();
            txtReferral.Text = x.PatientReferral.ToString();
            txtReason.Text = x.PatientReferralReason.ToString();
            txtZip.Text = x.PatientZipcode.ToString();
            if (x.PatientComments != null)
            {
                txtComments.Text = x.PatientComments.ToString();
            }
            lbSessions.ItemsSource = mySessions;
            if (x.PatientIsCompany)
            {
                cmbCompany.SelectedIndex = 1;
                txtCompany.Text = x.PatientCompanyNumber.ToString();
            }
            else
            {
                cmbCompany.SelectedIndex = 0;
            }
        }
    }
}
