namespace PsyAppVer2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Session")]
    public partial class Session
    {
        public int SessionID { get; set; }

        public DateTime SessionStart { get; set; }

        public DateTime SessionEnd { get; set; }
       
        [Column(TypeName = "date")]
        public DateTime SessionDate { get; set; }

        [NotMapped]
        public double? SessionPlannedDuration
        {
            get { 
                if( SessionStart != null && SessionEnd != null )
                {
                    return (SessionEnd - SessionStart).TotalMinutes / 15;
                }
                else
                {
                    return 0;
                }
            }                         
        }

        public decimal? SessionPrice { get; set; }

        public bool SessionIsPaid { get; set; }

        [StringLength(50)]
        public string SessionPaymentMethod { get; set; }

        [StringLength(50)]
        public string SessionSubject { get; set; }

        [StringLength(600)]
        public string SessionComments { get; set; }

        public int PatientID { get; set; }

        public int DayID { get; set; }

        public int? InvoiceID { get; set; }

        public int UserID { get; set; }

        public virtual Day Day { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual Patient Patient { get; set; }

        public virtual User User { get; set; }       

        public override string ToString()
        {
            return $"{SessionStart}       {SessionSubject}      {SessionComments}     {SessionPlannedDuration * 15}minuten      {SessionPrice} euro";
        }
    }
}
