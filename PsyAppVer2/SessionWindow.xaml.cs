﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyAppVer2
{
    /// <summary>
    /// Interaction logic for SessionWindow.xaml
    /// </summary>
    public partial class SessionWindow : Window
    {
        User myUser = new User();
        List<Session> mySessions = new List<Session>();
        List<Patient> myPatients = new List<Patient>();
        List<Day> myDays = new List<Day>();
        List<Session> selectedSessions = new List<Session>();
        DateTime today = DateTime.Now;
        public SessionWindow(User myUser)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.myUser = myUser;
            mySessions = DataManager.GetAllSessions();
            mySessions = mySessions.OrderBy(x => x.SessionStart).ToList();
            myPatients = DataManager.GetAllPatients();
            myDays = DataManager.GetAllDays();
            dtpDateInput.SelectedDate = today;
            cmbPatient.ItemsSource = myPatients;
            lbSessionList.ItemsSource = mySessions;
        }
        private void btnDateTimeSearch_Click(object sender, RoutedEventArgs e)
        {
            DateTime? selectedDate = dtpDateInput.SelectedDate as DateTime?;
            if (selectedDate != null)
            {
                lbSessionList.ItemsSource = null;
                mySessions = DataManager.GetSessionsByDate((DateTime)selectedDate);
                mySessions = mySessions.OrderBy(x => x.SessionStart).ToList();
                lbSessionList.ItemsSource = mySessions;
                if (mySessions.Count == 0)
                {
                    MessageBox.Show("Geen Sessies op deze Datum", "Error", MessageBoxButton.OK);
                }
            }
            else
            {
                MessageBox.Show("Gelieve een Datum te selecteren", "Error", MessageBoxButton.OK);
            }
        }
        private void btnSearchPatient_Click(object sender, RoutedEventArgs e)
        {
            selectedSessions.Clear();
            if (cmbPatient.SelectedItem != null)
            {
                foreach (Session session in mySessions)
                {
                    Patient myPatient = cmbPatient.SelectedItem as Patient;
                    if (session.PatientID == myPatient.PatientID)
                    {
                        selectedSessions.Add(session);
                    }
                }
                if (selectedSessions.Count > 0)
                {
                    lbSessionList.ItemsSource = null;
                    lbSessionList.ItemsSource = selectedSessions;
                }
                else
                {
                    MessageBox.Show("Geen Sessies gevonden voor Patient");
                }
            }
            else
            {
                MessageBox.Show("Gelieve een patient te selecteren", "Error", MessageBoxButton.OK);
            }
        }
        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDetailSession_Click(object sender, RoutedEventArgs e)
        {
            if (lbSessionList.SelectedItem != null)
            {
                SessionDetailWindow mySessionDetailWindow = new SessionDetailWindow(lbSessionList.SelectedItem as Session, myUser);
                mySessionDetailWindow.ShowDialog();

            }
            else
            {
                MessageBox.Show("Gelieve een Sessie te selecteren", "Hola", MessageBoxButton.OK);
            }
        }

        private void btnUpdateSession_Click(object sender, RoutedEventArgs e)
        {
            if (lbSessionList.SelectedItem != null)
            {
                SessionUpdateWindow mySessionUpdateWindow = new SessionUpdateWindow(lbSessionList.SelectedItem as Session, myUser);
                mySessionUpdateWindow.ShowDialog();
            }
            else
            {
                MessageBox.Show("Gelieve een Sessie te selecteren", "Hola", MessageBoxButton.OK);
            }
        }
        private void btnDeleteSession_Click(object sender, RoutedEventArgs e)
        {
            if (lbSessionList.SelectedItem != null)
            {
                Session mySession = lbSessionList.SelectedItem as Session;
                if (MessageBox.Show($"Wilt u Sessie {mySession.SessionDate} {mySession.SessionSubject} verwijderen?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    DataManager.DeleteSession(mySession);
                    MessageBox.Show("Succesvol verwijderd", "Geslaagd", MessageBoxButton.OK);
                    lbSessionList.ItemsSource = null;
                    mySessions = DataManager.GetAllSessions();
                    lbSessionList.ItemsSource = mySessions;
                }
            }
            else
            {
                MessageBox.Show("Gelieve een Sessie te selecteren", "Hola", MessageBoxButton.OK);
            }
        }
        private void btnCreateSession_Click(object sender, RoutedEventArgs e)
        {
            SessionCreateWindow thisSessionCreateWindow = new SessionCreateWindow(myUser);
            thisSessionCreateWindow.ShowDialog();
            if (thisSessionCreateWindow.DialogResult.HasValue && thisSessionCreateWindow.DialogResult.Value)
            {
                MessageBox.Show($"Sessie Aangemaakt voor Patient", "Succes", MessageBoxButton.OK);
                lbSessionList.ItemsSource = null;
                mySessions = DataManager.GetAllSessions();
                mySessions = mySessions.OrderBy(x => x.SessionStart).ToList();
                lbSessionList.ItemsSource = mySessions;
            }
        }
    }
}
