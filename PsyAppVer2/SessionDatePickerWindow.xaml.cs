﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyAppVer2
{
    /// <summary>
    /// Interaction logic for SessionDatePickerWindow.xaml
    /// </summary>
    public partial class SessionDatePickerWindow : Window
    {
        Session mySession = new Session();
        Day myDay = new Day();
        List<Day> myDays = new List<Day>();
        string[] arrDagindeling = new string[64];

        public SessionDatePickerWindow(Session x)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            mySession = x;
            myDays = DataManager.GetAllDays();
            dtpDatepicker.SelectedDate = DateTime.Now;
            ResetButtons();
        }

        private void btnChooseDate_Click(object sender, RoutedEventArgs e)
        {
            lbDaySchedule.ItemsSource = null;
            bool dayExists = false;
            DateTime pickedDateTime = ((DateTime)((DateTime)dtpDatepicker.SelectedDate as DateTime?));
            var pickedDate = pickedDateTime.Date;           
            DateTime thisDateTime = new DateTime();
            thisDateTime = pickedDateTime.AddHours(8);
            List<Session> mySessions = DataManager.GetSessionsByDate(pickedDate);
            foreach (Day x in myDays)
            {
                if (x.DayDate.Date == pickedDate)
                {
                    this.myDay = x;
                    dayExists = true;
                    mySession.Day = myDay;
                    mySession.DayID = myDay.DayID;
                    mySession.SessionDate = pickedDate;
                }
            }
            if (!dayExists)
            {
                myDay.DayDate = pickedDate;
                DataManager.CreateDay(myDay);
                myDays = DataManager.GetAllDays();
                myDay = myDays.FirstOrDefault(x => x.DayDate.Date == pickedDate);
                mySession.Day = myDay;
                mySession.DayID = myDay.DayID;
                mySession.SessionDate = pickedDate;
            }
            for (int i = 0; i < arrDagindeling.Length; i++)
            {
                foreach (Session x in mySessions)
                {
                    int counter = 1;
                    //if (x.SessionReservedBefore == thisDateTime)
                    //{
                    //    arrDagindeling[i] = $"{thisDateTime.ToString("HH:mm")}: Bezet, Gereserveerd voor start van : {x.SessionSubject}";
                    //    for (int j = 1; j < x.SessionPlannedDuration; j++)
                    //    {
                    //        arrDagindeling[(i + j)] = $"{(thisDateTime.AddMinutes(counter * 15)).ToString("HH:mm")}: Bezet, {x.SessionSubject}";
                    //        counter++;
                    //    }
                    //}                                      
                }
                thisDateTime = thisDateTime.AddMinutes(15);
                if (arrDagindeling[i] == null || arrDagindeling[i] == "")
                {
                    arrDagindeling[i] = $"{thisDateTime.ToString("HH:mm")}: VRIJ";
                    cmbSessionStart.Items.Add(thisDateTime);                   
                }
            }
            lbDaySchedule.ItemsSource = arrDagindeling;
            dtpDatepicker.IsEnabled = false;
            btnChooseDate.IsEnabled = false;
            cmbSessionStart.IsEnabled = true;
            btnConfirmStart.IsEnabled = true;
        }
        private void btnConfirmStart_Click(object sender, RoutedEventArgs e)
        {
            if (cmbSessionStart.SelectedIndex != -1)
            {
                mySession.SessionStart = (DateTime)cmbSessionStart.SelectedItem;
                lbDaySchedule.ItemsSource = null;
                int counter = 1;
                int counter2 = 1;
                for (int i = 0; i < arrDagindeling.Length; i++)
                {
                    if ((mySession.SessionStart.ToString("HH:mm")) == arrDagindeling[i].Substring(0, 5))
                    {
                        arrDagindeling[i] = $"{mySession.SessionStart.ToString("HH:mm")} : Bezet, Gereserveerd voor huidige Sessie.";
                        for (int j = i + 1; j < arrDagindeling.Length; j++)
                        {
                            if (arrDagindeling[j].Substring(7, 4) == "VRIJ")
                            {
                                cmbSessionEnd.Items.Add(mySession.SessionStart.AddMinutes(counter * 15));
                                counter++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        for (int z = i - 1; z >= 0; z--)
                        {
                            if (arrDagindeling[z].Substring(7, 4) == "VRIJ")
                            {
                                cmbReservedBefore.Items.Add(mySession.SessionStart.AddMinutes(counter2 * -15));
                                counter2++;
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                lbDaySchedule.ItemsSource = arrDagindeling;
                cmbSessionEnd.IsEnabled = true;
                btnConfirmEnd.IsEnabled = true;
                btnConfirmStart.IsEnabled = false;
                cmbSessionStart.IsEnabled = false;
            }
            else
            {
                MessageBox.Show("Gelieve een optie te selecteren", "Holaba", MessageBoxButton.OK);
            }
            
        }

        private void btnConfirmEnd_Click(object sender, RoutedEventArgs e)
        {
            if ( cmbSessionEnd.SelectedIndex != -1)
            {
                mySession.SessionEnd = (DateTime)cmbSessionEnd.SelectedItem;
                lbDaySchedule.ItemsSource = null;
                int counter = 1;
                for (int i = 0; i < arrDagindeling.Length; i++)
                {
                    if ((mySession.SessionEnd.ToString("HH:mm") == arrDagindeling[i].Substring(0, 5)))
                    {
                        arrDagindeling[i - 1] = $"{mySession.SessionEnd.ToString("HH:mm")} : Einde Huidige Sessie.";
                        for (int j = i; j < arrDagindeling.Length; j++)
                        {
                            if (arrDagindeling[j].Substring(7, 4) == "VRIJ")
                            {
                                cmbReservedAfter.Items.Add(mySession.SessionEnd.AddMinutes(counter * 15));
                                counter++;
                            }
                            else
                            {
                                break;
                            }
                        }
                        break;
                    }
                }
                for (int i = 0; i < arrDagindeling.Length; i++)
                {
                    if (mySession.SessionStart.ToString("HH:mm") == arrDagindeling[i].Substring(0, 5))
                    {
                        for (int j = i + 1; j < arrDagindeling.Length; j++)
                        {
                            if (arrDagindeling[j].Substring(7, 4) == "VRIJ")
                            {
                                arrDagindeling[j] = $"Bezet, Gereserveerd voor huidige Sessie.";
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                lbDaySchedule.ItemsSource = arrDagindeling;
                cmbSessionEnd.IsEnabled = false;
                btnConfirmEnd.IsEnabled = false;
                btnReservedBefore.IsEnabled = true;
                cmbReservedBefore.IsEnabled = true;
                btnConfirm.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Gelieve een optie te selecteren", "Ola Maat", MessageBoxButton.OK);
            }
            
        }

        private void btnReservedBefore_Click(object sender, RoutedEventArgs e)
        {
            
            if (cmbReservedBefore.SelectedIndex != -1)
            {
                lbDaySchedule.ItemsSource = null;
                //mySession.SessionReservedBefore = (DateTime)cmbReservedBefore.SelectedItem;
                DateTime tempdatetime = (DateTime)cmbReservedBefore.SelectedItem;
                for (int i = 0; i < arrDagindeling.Length; i++)
                {
                    if (tempdatetime.ToString("HH:mm") == arrDagindeling[i].Substring(0, 5))
                    {
                        for (int j = i; j < arrDagindeling.Length; j++)
                        {
                            if (arrDagindeling[j].Substring(7, 4) == "VRIJ")
                            {
                                arrDagindeling[j] = $"Bezet, Gereserveerd voor huidige Sessie.";
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                
                lbDaySchedule.ItemsSource = arrDagindeling;
            }
            else
            {
                //mySession.SessionReservedBefore = null;
            }
            btnReservedBefore.IsEnabled = false;
            cmbReservedBefore.IsEnabled = false;
            cmbReservedAfter.IsEnabled = true;
            btnReservedAfter.IsEnabled = true;
        }

        private void btnReservedAfter_Click(object sender, RoutedEventArgs e)
        {
            
            if (cmbReservedAfter.SelectedIndex != -1)
            {
                lbDaySchedule.ItemsSource = null;
                //mySession.SessionReservedAfter = (DateTime)cmbReservedAfter.SelectedItem;
                DateTime tempdatetime = (DateTime)cmbReservedAfter.SelectedItem;
                for (int i = 0; i < arrDagindeling.Length; i++)
                {
                    if (tempdatetime.ToString("HH:mm") == arrDagindeling[i].Substring(0, 5))
                    {
                        for (int j = i - 1; j < arrDagindeling.Length; j--)
                        {
                            if (arrDagindeling[j].Substring(7, 4) == "VRIJ")
                            {
                                arrDagindeling[j] = $"Bezet, Gereserveerd voor huidige Sessie.";
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
                
            }
            else
            {
                //mySession.SessionReservedAfter = null;
            }
            lbDaySchedule.ItemsSource = arrDagindeling;
            cmbReservedAfter.IsEnabled = false;
            btnReservedAfter.IsEnabled = false;
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            ResetButtons();
            arrDagindeling = null;
            arrDagindeling = new string[64];
        }

        private void btnMenu_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            this.Close();
        }

        private void ResetButtons()
        {
            dtpDatepicker.IsEnabled = true;
            btnChooseDate.IsEnabled = true;
            cmbReservedAfter.IsEnabled = false;
            cmbReservedBefore.IsEnabled = false;
            cmbSessionEnd.IsEnabled = false;
            cmbSessionStart.IsEnabled = false;
            btnConfirmEnd.IsEnabled = false;
            btnConfirmStart.IsEnabled = false;
            btnReservedBefore.IsEnabled = false;
            btnReservedAfter.IsEnabled = false;
            lbDaySchedule.ItemsSource = null;
            cmbReservedAfter.Items.Clear();
            cmbReservedBefore.Items.Clear();
            cmbSessionEnd.Items.Clear();
            cmbSessionStart.Items.Clear();
            btnConfirm.IsEnabled = false;
        }
    }
}
