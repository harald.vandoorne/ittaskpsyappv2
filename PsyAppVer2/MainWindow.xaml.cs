﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PsyAppVer2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        bool checkDBConnection = false;
        User myUser = new User();
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //Test DB connection
            using (var PsyAppDBEntities = new PysAppDBModel())
            {
                checkDBConnection = PsyAppDBEntities.Database.Exists();
            }
            if (checkDBConnection)
            {
                Login thisLogin = new Login(myUser);
                thisLogin.ShowDialog();
                if (thisLogin.DialogResult.HasValue && thisLogin.DialogResult.Value)
                {
                    myUser = thisLogin.myUser;
                }
                else
                {
                    Application.Current.Shutdown();
                }
            }
            else
            {
                MessageBox.Show("Could not connect to the Database on server '.\\SQLEXPRESS'.\nPlease check that the Database exists.", "Database Connection Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                Application.Current.Shutdown();
            }
        }

        private void btnPatients_Click(object sender, RoutedEventArgs e)
        {
            PatientWindow thisPatientwindow = new PatientWindow(myUser);
            thisPatientwindow.ShowDialog();
        }

        private void btnSessions_Click(object sender, RoutedEventArgs e)
        {
            SessionWindow thisSessionWindow = new SessionWindow(myUser);
            thisSessionWindow.ShowDialog();
        }

        private void btnAgenda_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnFacturen_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}

