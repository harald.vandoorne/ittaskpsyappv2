using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace PsyAppVer2
{
    public partial class PysAppDBModel : DbContext
    {
        public PysAppDBModel()
            : base("name=PysAppDBModel")
        {
        }

        public virtual DbSet<Day> Day { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }
        public virtual DbSet<Patient> Patient { get; set; }
        public virtual DbSet<Session> Session { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Day>()
                .HasMany(e => e.Session)
                .WithRequired(e => e.Day)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Patient>()
                .HasMany(e => e.Session)
                .WithRequired(e => e.Patient)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Session>()
                .Property(e => e.SessionPrice)
                .HasPrecision(18, 0);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Session)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);
        }
    }
}
