﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyAppVer2
{
    /// <summary>
    /// Interaction logic for SessionCreateWindow.xaml
    /// </summary>
    public partial class SessionCreateWindow : Window
    {
        Session mySession = new Session();
        Patient myPatient = new Patient();
        Day myDay = new Day();
        List<Day> myDays = new List<Day>();
        List<Patient> myPatients = new List<Patient>();
        List<Session> mySessions = new List<Session>();

        public SessionCreateWindow(User x)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            mySession.User = x;
            myDays = DataManager.GetAllDays();
            dtpSelectDay.SelectedDate = DateTime.Now;
            myPatients = DataManager.GetAllPatients();
            cmbPatientPicker.ItemsSource = myPatients;
            ResetButtons();
            LoadComboBoxes();

        }
        public SessionCreateWindow(Patient x, User y)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            myPatient = x;
            mySession.User = y;
            myDays = DataManager.GetAllDays();
            mySession.Patient = x;
            dtpSelectDay.SelectedDate = DateTime.Now;
            cmbPatientPicker.Items.Add(x);
            cmbPatientPicker.SelectedIndex = 0;
            ResetButtons();
            LoadComboBoxes();
        }

        public SessionCreateWindow(Day x)
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            this.myDay = x;
            mySession.Day = x;
            myDays = DataManager.GetAllDays();
            dtpSelectDay.SelectedDate = myDay.DayDate;           
            ResetButtons();
            LoadComboBoxes();
        }
        
        private void btnNextAvailableDate_Click(object sender, RoutedEventArgs e)
        {
            SessionDatePickerWindow thisSessionDatePicker = new SessionDatePickerWindow(mySession);
            thisSessionDatePicker.ShowDialog();
            if (thisSessionDatePicker.DialogResult.HasValue && thisSessionDatePicker.DialogResult.Value)
            {
                dtpSelectDay.SelectedDate = mySession.SessionDate;
                btnSelectDate.IsEnabled = false;
                cmbPatientPicker.IsEnabled = true;
                btnPatientConfirm.IsEnabled = true;
            }

        }

        private void btnPatientConfirm_Click(object sender, RoutedEventArgs e)
        {
            if (cmbPatientPicker.SelectedIndex != -1)
            {

                myPatient = cmbPatientPicker.SelectedItem as Patient;
                mySession.Patient = myPatient;
                mySession.PatientID = myPatient.PatientID;                
                cmbPaymentMethod.IsEnabled = true;
                cmbPaymentStatus.IsEnabled = true;
                txtPrice.IsEnabled = true;
                txtSubject.IsEnabled = true;
                txtComments.IsEnabled = true;               
                cmbPatientPicker.IsEnabled = false;
                dtpSelectDay.IsEnabled = false;                                
                btnPatientConfirm.IsEnabled = false;
                btnCreate.IsEnabled = true;
            }
            else
            {
                MessageBox.Show("Gelieve een Patient te kiezen", "Error", MessageBoxButton.OK);
            }
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            mySession.SessionComments = txtComments.Text;
            mySession.SessionSubject = txtSubject.Text;           
            Decimal.TryParse(txtPrice.Text, out decimal myPrice);
            mySession.SessionPrice = myPrice;
            if (cmbPaymentStatus.SelectedIndex == 0)
            {
                mySession.SessionIsPaid = false;
            }
            else
            {
                mySession.SessionIsPaid = true;
            }
            mySession.SessionPaymentMethod = cmbPaymentMethod.Text;           
            DataManager.CreateSession(mySession);
            mySessions = DataManager.GetAllSessions();
            DialogResult = true;
            this.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
       
        private void ResetButtons()
        {           
            cmbPatientPicker.IsEnabled = false;
            btnPatientConfirm.IsEnabled = false;    
            txtPrice.IsEnabled = false;
            txtComments.IsEnabled = false;
            txtSubject.IsEnabled = false;
            btnCreate.IsEnabled = false;
            cmbPaymentMethod.IsEnabled = false;
            cmbPaymentStatus.IsEnabled = false;
        }

        private void LoadComboBoxes()
        {
            cmbPaymentMethod.Items.Add("Cash");
            cmbPaymentMethod.Items.Add("Bankkaart");
            cmbPaymentMethod.Items.Add("Overschrijving");
            cmbPaymentMethod.Items.Add("Andere");
            cmbPaymentStatus.Items.Add("Niet Betaald");
            cmbPaymentStatus.Items.Add("Betaald");          
        }
    }
}
