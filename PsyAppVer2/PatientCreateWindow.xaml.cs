﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PsyAppVer2
{
    /// <summary>
    /// Interaction logic for PatientCreateWindow.xaml
    /// </summary>
    public partial class PatientCreateWindow : Window
    {
        Patient myPatient = new Patient();
        List<Patient> myPatients = new List<Patient>();
        public PatientCreateWindow()
        {
            InitializeComponent();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            myPatients = DataManager.GetAllPatients();
            cmbCompany.Items.Add("Privé");
            cmbCompany.Items.Add("Bedrijf");
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            if (txtFamilienaam.Text != "" && txtVoornaam.Text != "")
            {
                myPatient.PatientCity = txtCity.Text;
                myPatient.PatientStreet = txtStreet.Text;
                myPatient.PatientFirstName = txtVoornaam.Text;
                myPatient.PatientLastName = txtFamilienaam.Text;
                myPatient.PatientHouseNumber = txtNumber.Text;
                myPatient.PatientPhone = txtPhone.Text;
                myPatient.PatientEmail = txtEmail.Text;
                myPatient.PatientReferral = txtReferral.Text;
                myPatient.PatientReferralReason = txtReason.Text;
                myPatient.PatientZipcode = txtZip.Text;
                myPatient.PatientComments = "";
                if (cmbCompany.SelectedIndex == 1)
                {
                    myPatient.PatientIsCompany = true;
                    myPatient.PatientCompanyNumber = txtCompany.Text;
                }
                else
                {
                    myPatient.PatientIsCompany = false;
                }
                bool check = false;
                foreach (Patient x in myPatients)
                {
                    if (x.PatientEmail == txtEmail.Text)
                    {
                        MessageBox.Show("Patient met deze email bestaat al", "Hola", MessageBoxButton.OK);
                        check = true;
                    }
                }
                if (!check)
                {
                    DataManager.CreatePatient(myPatient);
                    this.DialogResult = true;
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Voornaam + Familienaam dienen ingevuld te zijn", "Hola zeker", MessageBoxButton.OK);
            }
        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
