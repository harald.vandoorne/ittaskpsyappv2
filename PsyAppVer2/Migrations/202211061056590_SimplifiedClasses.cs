﻿namespace PsyAppVer2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SimplifiedClasses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlockedMoments",
                c => new
                    {
                        BlockedMomentID = c.Int(nullable: false, identity: true),
                        DayDate = c.DateTime(nullable: false),
                        BlockedStart = c.DateTime(nullable: false),
                        BlockedEnd = c.DateTime(nullable: false),
                        BlockedReason = c.String(),
                        Day_DayID = c.Int(),
                    })
                .PrimaryKey(t => t.BlockedMomentID)
                .ForeignKey("dbo.Day", t => t.Day_DayID)
                .Index(t => t.Day_DayID);
            
            DropColumn("dbo.Session", "SessionReservedBefore");
            DropColumn("dbo.Session", "SessionReservedAfter");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Session", "SessionReservedAfter", c => c.DateTime());
            AddColumn("dbo.Session", "SessionReservedBefore", c => c.DateTime());
            DropForeignKey("dbo.BlockedMoments", "Day_DayID", "dbo.Day");
            DropIndex("dbo.BlockedMoments", new[] { "Day_DayID" });
            DropTable("dbo.BlockedMoments");
        }
    }
}
