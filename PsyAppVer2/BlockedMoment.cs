﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PsyAppVer2
{

    
    public partial class BlockedMoment
    {
        public int BlockedMomentID { get; set; }

        
        public DateTime DayDate { get; set; }

        
        public DateTime BlockedStart { get; set; }

        
        public DateTime BlockedEnd { get; set; }

        
        public string BlockedReason { get; set; }

        public virtual Day Day { get; set; }
    }
}
